package tarot.infrastructure

import akka.actor.{ActorRef, ActorSystem}
import akka.testkit.TestProbe
import tarot.business.{Deck, Player, TarotCard}
import tarot.infrastructure.GameNightSupervisor.PlayerMessage
import org.scalatest.BeforeAndAfterAll
import org.scalatestplus.play.PlaySpec
import play.api.libs.json.Json

import scala.concurrent.duration._
import scala.util.Random


class GameNightSupervisorSpec extends PlaySpec with BeforeAndAfterAll {

  "Given a deck of cards, game night" should {
    "handle a full game" in {

      implicit val system: ActorSystem = ActorSystem()

      val random = getSameRandom
      val cards = random.shuffle(TarotCard.values.toList.slice(40, 55))
      val deck = Deck(cards, random)
      val madeleine = Player("Madeleine")
      val viktor = Player("Viktor")
      val mathieu = Player("Mathieu")
      val players = Map(
        madeleine -> TestProbe(),
        viktor -> TestProbe(),
        mathieu -> TestProbe())
      val supervisor: ActorRef = system.actorOf(GameNightSupervisor.props(deck))

      val viewMessages = (numberOfMessages: Int) => {
        val duration = 50.seconds
        //        println("--- Madeleine's messages")
        //        players(madeleine).receiveN(numberOfMessages, duration).foreach(println(_))
        //        players(madeleine).expectNoMessage()
        //
        //        println("--- Viktor's messages")
        //        players(viktor).receiveN(numberOfMessages, duration).foreach(println(_))
        //        players(viktor).expectNoMessage()

        println("--- Mathieu's messages")
        players(mathieu).receiveN(numberOfMessages, duration).foreach(println(_))
        players(mathieu).expectNoMessage()
      }

      players.foreach(player => supervisor.tell(
        PlayerMessage(player._1, Json.obj("action" -> "join")), player._2.ref))
      supervisor ! PlayerMessage(mathieu, Json.obj("action" -> "start-game"))

      // Bid
      supervisor ! PlayerMessage(madeleine, Json.parse("""{"action" : "bid", "bid-value": "PASS"}"""))
      supervisor ! PlayerMessage(viktor, Json.parse("""{"action" : "bid", "bid-value": "PASS"}"""))
      supervisor ! PlayerMessage(mathieu, Json.parse("""{"action" : "bid", "bid-value": "GARDE"}"""))

      viewMessages(20)

      // Dog
      supervisor ! PlayerMessage(mathieu, Json.parse("""{"action" : "update-dog", "cards": ["CLUB_ONE", "CLUB_THREE", "CLUB_FIVE", "DIAMOND_SIX", "DIAMOND_SEVEN", "DIAMOND_EIGHT"]}"""))
      viewMessages(3)

      // Play
      supervisor ! PlayerMessage(madeleine, Json.parse("""{"action": "move", "card": "DIAMOND_JACK"} """))
      viewMessages(2)
      supervisor ! PlayerMessage(viktor, Json.parse("""{"action": "move", "card": "DIAMOND_KING"} """))
      viewMessages(2)
      supervisor ! PlayerMessage(mathieu, Json.parse("""{"action": "move", "card": "DIAMOND_FIVE"} """))
      viewMessages(2)

      // Next rounds
      supervisor ! PlayerMessage(viktor, Json.parse("""{"action": "move", "card": "CLUB_FOUR"} """))
      viewMessages(2)
      supervisor ! PlayerMessage(mathieu, Json.parse("""{"action": "move", "card": "DIAMOND_NINE"} """))
      viewMessages(2)
      supervisor ! PlayerMessage(madeleine, Json.parse("""{"action": "move", "card": "DIAMOND_TEN"} """))
      viewMessages(2)
      supervisor ! PlayerMessage(viktor, Json.parse("""{"action": "move", "card": "CLUB_TWO"} """))
      viewMessages(2)
      supervisor ! PlayerMessage(mathieu, Json.parse("""{"action": "move", "card": "DIAMOND_KNIGHT"} """))
      viewMessages(2)
      supervisor ! PlayerMessage(madeleine, Json.parse("""{"action": "move", "card": "DIAMOND_QUEEN"} """))
      viewMessages(2)

    }
  }

  private def getSameRandom = {
    val random = Random

    random.setSeed(1)
    random
  }
}