package tarot.business

import tarot.business.GreenMat.WinStack
import tarot.business.Player.{Hand, PlayerMove}
import tarot.business.TarotCard.{TarotCard, _}
import org.mockito.scalatest.MockitoSugar
import org.scalatestplus.play.PlaySpec

class GameTrickSpec extends PlaySpec with MockitoSugar {

  private val player1 = Player("1")
  private val player2 = Player("2")
  private val player3 = Player("3")

  "When a player moves, GameTrick" should {
    val greenMat = mock[GreenMat]
    val gameTrick = GameTrick(List(Hand(player1, List()), Hand(player2, List())), List(), greenMat)
    when(greenMat.validatePlayerMove(any[PlayerMove], any[Hand])).thenReturn(true)

    "validates it's the player's turn" in {
      val playerHand = Hand(player1, List())
      val playerMove = PlayerMove(player1, mock[TarotCard])
      val target = gameTrick.validatePlayerMove(playerMove)

      target mustBe true
      verify(greenMat).validatePlayerMove(playerMove, playerHand)
    }

    "validates it's not the player's turn" in {
      val target = gameTrick.validatePlayerMove(PlayerMove(player2, mock[TarotCard]))

      target mustBe false
    }
  }

  "When a player moves, GameTrick" should {
    val greenMat = GreenMat(numberOfPlayers = 3)
    val hands = List(Hand(player1, List(HEART_KING)), Hand(player2, List(HEART_QUEEN)), Hand(player3, List(SPADE_EIGHT)))
    val gameTrick = GameTrick(hands, List(), greenMat)

    "updates the player hands for one player movement" in {
      val playerMove = PlayerMove(player1, HEART_KING)

      val target = gameTrick.playerMove(playerMove)

      target.isDefined mustBe true
      target.get.playerHands.head.player mustBe player2
      target.get.playerHands.last.cards.isEmpty mustBe true
    }

    "updates the win stacks when the last player moved" in {

      val target = gameTrick.playerMove(PlayerMove(player1, HEART_KING))
        .get.playerMove(PlayerMove(player2, HEART_QUEEN))
        .get.playerMove(PlayerMove(player3, SPADE_EIGHT))

      target.isDefined mustBe true
      target.get.winStacks.head.player mustBe player1
      target.get.winStacks.head.cards.size mustBe 3
    }

  }
}
