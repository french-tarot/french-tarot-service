package tarot.business

import tarot.business.BiddingRound.{Bid, BidValue}
import tarot.business.BiddingRoundSpec._
import tarot.business.Player.Hand
import org.mockito.scalatest.MockitoSugar
import org.scalatestplus.play.PlaySpec

class BiddingRoundSpec extends PlaySpec with MockitoSugar {

  "Given three players, a BiddingRound" should {

    "be empty if they all passed" in {
      val biddingRound = BiddingRound(players.map(Hand(_, List())), players.map(Bid(_, BidValue.PASS)))

      val target = biddingRound.getWinningBid

      target.isEmpty mustBe true
    }

    "accept a new valid bid" in {
      val biddingRound = BiddingRound(players.map(Hand(_, List())),
        List(Bid(players.head, BidValue.PASS), Bid(players(1), BidValue.GARDE)))

      val target = biddingRound.placeNextBid(players(2), BidValue.PASS)

      target.isDefined mustBe true
      target.get.playerBids.size mustBe 3
      target.get.playerBids.last.value mustBe BidValue.PASS
      target.get.playerBids.last.player mustBe players(2)
    }

    "reject an invalid bid" in {
      val biddingRound = BiddingRound(players.map(Hand(_, List())),
        List(Bid(players.head, BidValue.PASS)))

      val target = biddingRound.placeNextBid(players(2), BidValue.PASS)

      target.isEmpty mustBe true
    }

    "decide bid winner as leader" in {
      val biddingRound = BiddingRound(players.map(Hand(_, List())),
        List(Bid(players.head, BidValue.PASS), Bid(players(1), BidValue.GARDE), Bid(players(2), BidValue.PASS)))

      val target = biddingRound.getWinningBid

      target.isDefined mustBe true
      target.get.player mustBe players(1)
      target.get.value mustBe BidValue.GARDE
    }
  }
}

object BiddingRoundSpec {
  private val players = List(Player("1"), Player("2"), Player("3"))

}