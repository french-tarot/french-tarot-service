package tarot.business

import tarot.business.TarotCard._
import org.mockito.scalatest.MockitoSugar
import org.scalatestplus.play.PlaySpec
import DeckSpec._
import scala.util.Random

class DeckSpec extends PlaySpec with MockitoSugar {

  "Given a list of cards, a Deck" should {

    "distribute them among all three players" in {
      val deck = Deck(cards, randomSeed)

      val target = deck.distributeCards(players.slice(0,3))

      target.dog.cards.size mustBe 6
      target.playerHands.size mustBe 3
      target.playerHands.last.player mustBe players(2)
      target.playerHands.last.cards.last mustBe TRUMP_SIX
    }

    "distribute them among all five players" in {
      val deck = Deck(cards, randomSeed)

      val target = deck.distributeCards(players)

      target.dog.cards.size mustBe 3
      target.playerHands.size mustBe 5
      target.playerHands.last.player mustBe players(3)
      target.playerHands.last.cards.last mustBe TRUMP_SIX
    }

  }

}

object DeckSpec {
  private val players = List(Player("1"), Player("2"), Player("3"), Player("4"), Player("5"))
  private val randomSeed = Random

  randomSeed.setSeed(0)

  private val cards = List(HEART_ONE, HEART_TWO, HEART_THREE, HEART_FOUR, HEART_FIVE,
    HEART_SIX, HEART_SEVEN, HEART_EIGHT, HEART_NINE, TRUMP_ONE, TRUMP_TWO, TRUMP_THREE, TRUMP_FOUR, TRUMP_FIVE,
    TRUMP_SIX)
}