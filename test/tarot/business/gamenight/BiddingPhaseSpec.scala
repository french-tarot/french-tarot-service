package tarot.business.gamenight

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import tarot.business.{BiddingRound, Player, ScoreBoard, TarotCard}
import tarot.business.BiddingRound.{Bid, BidValue}
import tarot.business.Deck.Dog
import tarot.business.Player.Hand
import tarot.business.gamenight.GameNight.{GetPhaseName, GetPlayerHand, PhaseName, PlaceBid, PlacedBid, ReceivedPhaseName, ReceivedPlayerHand}
import org.mockito.scalatest.MockitoSugar
import org.scalatest.BeforeAndAfterAll
import org.scalatestplus.play.PlaySpec

class BiddingPhaseSpec extends PlaySpec with BeforeAndAfterAll with MockitoSugar {

  private val testKit = ActorTestKit()

  override def afterAll(): Unit = testKit.shutdownTestKit()

  "At the beginning of the bidding round, game" should {

    "prevent wrong player from bidding" in {
      val dog = mock[Dog]
      val biddingRound = mock[BiddingRound]
      val player = Player("player1")
      val bidValue = BidValue.GARDE
      val testProbe = testKit.createTestProbe[PlacedBid]
      val target = testKit.spawn(BiddingPhase(dog, mock[ScoreBoard]).nextBid(biddingRound))

      when(biddingRound.placeNextBid(player, bidValue)).thenReturn(Option.empty)

      target ! PlaceBid(player, bidValue, testProbe.ref)

      testProbe.expectMessage(PlacedBid(player, bidValue, isBidValid = false))
      verify(biddingRound).placeNextBid(player, bidValue)
    }

    "show a player's hand" in {
      val dog = mock[Dog]
      val biddingRound = mock[BiddingRound]
      val player = Player("player1")
      val testProbe = testKit.createTestProbe[ReceivedPlayerHand]
      val target = testKit.spawn(BiddingPhase(dog, mock[ScoreBoard]).nextBid(biddingRound))
      val playerHand = Hand(player, List(TarotCard.CLUB_EIGHT))

      when(biddingRound.playerHands).thenReturn(List(playerHand))

      target ! GetPlayerHand(player, testProbe.ref)

      testProbe.expectMessage(ReceivedPlayerHand(playerHand))
    }
  }

  "Given a round, game" should {
    val dog = mock[Dog]
    val player = Player("player1")
    val bidValue = BidValue.GARDE
    val phaseProbe = testKit.createTestProbe[ReceivedPhaseName]

    "accept bid and move to next bid" in {
      val biddingRound = mock[BiddingRound]
      val nextBiddingRound = mock[BiddingRound]
      val testProbe = testKit.createTestProbe[PlacedBid]
      val target = testKit.spawn(BiddingPhase(dog, mock[ScoreBoard]).nextBid(biddingRound))

      when(biddingRound.placeNextBid(player, bidValue))
        .thenReturn(Option(nextBiddingRound))
      when(nextBiddingRound.isBiddingFinished).thenReturn(false)

      target ! PlaceBid(player, bidValue, testProbe.ref)

      testProbe.expectMessage(PlacedBid(player, bidValue, isBidValid = true))
      verify(biddingRound).placeNextBid(player, bidValue)

      target ! GetPhaseName(phaseProbe.ref)
      phaseProbe.expectMessage(ReceivedPhaseName(PhaseName.BIDDING_PHASE))
    }

    "accept bid and go to dog phase" in {
      val winningBid = Bid(player, bidValue)
      val biddingRound = mock[BiddingRound]
      val nextBiddingRound = BiddingRound(List(Hand(player, List())), List(winningBid))
      val testProbe = testKit.createTestProbe[PlacedBid]
      val target = testKit.spawn(BiddingPhase(dog, mock[ScoreBoard]).nextBid(biddingRound))

      when(biddingRound.placeNextBid(player, bidValue))
        .thenReturn(Option(nextBiddingRound))

      target ! PlaceBid(player, bidValue, testProbe.ref)

      testProbe.expectMessage(PlacedBid(player, bidValue, isBidValid = true))
      verify(biddingRound).placeNextBid(player, bidValue)

      target ! GetPhaseName(phaseProbe.ref)
      phaseProbe.expectMessage(ReceivedPhaseName(PhaseName.DOG_PHASE))
    }

    "accept bid and go to game phase" in {
      val winningBid = Bid(player, BidValue.GARDE_CONTRE)
      val biddingRound = mock[BiddingRound]
      val nextBiddingRound = BiddingRound(List(Hand(player, List())), List(winningBid))
      val testProbe = testKit.createTestProbe[PlacedBid]
      val target = testKit.spawn(BiddingPhase(dog, mock[ScoreBoard]).nextBid(biddingRound))

      when(biddingRound.placeNextBid(player, bidValue))
        .thenReturn(Option(nextBiddingRound))

      target ! PlaceBid(player, bidValue, testProbe.ref)

      testProbe.expectMessage(PlacedBid(player, bidValue, isBidValid = true))
      verify(biddingRound).placeNextBid(player, bidValue)

      target ! GetPhaseName(phaseProbe.ref)
      phaseProbe.expectMessage(ReceivedPhaseName(PhaseName.GAME_ROUND_PHASE))
    }


  }
}
