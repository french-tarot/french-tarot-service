package tarot.business.gamenight

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import tarot.business.BiddingRound.Bid
import tarot.business.Deck.Dog
import tarot.business.Player.Hand
import tarot.business.{Player, ScoreBoard, TarotCard}
import tarot.business.gamenight.GameNight.{GetPhaseName, PhaseName, ReceivedPhaseName, UpdateDog, UpdatedDog, ViewDog, ViewedDog}
import org.mockito.scalatest.MockitoSugar
import org.scalatest.BeforeAndAfterAll
import org.scalatestplus.play.PlaySpec

class DogPhaseSpec extends PlaySpec with BeforeAndAfterAll with MockitoSugar {

  private val testKit = ActorTestKit()
  private val phaseProbe = testKit.createTestProbe[ReceivedPhaseName]

  override def afterAll(): Unit = testKit.shutdownTestKit()

  "For a given dog, dog phase receiving a view request" should {
    "return an empty list if dog is hidden" in {
      val dog = mock[Dog]
      val winningBid = mock[Bid]
      val playerHands = List()
      val target = testKit.spawn(DogPhase(dog, winningBid, playerHands, mock[ScoreBoard]))
      val testProbe = testKit.createTestProbe[ViewedDog]
      val player = Player("Tester")

      when(winningBid.shouldDogBeVisible).thenReturn(false)
      when(winningBid.player).thenReturn(player)

      target ! ViewDog(testProbe.ref)

      testProbe.expectMessage(ViewedDog(isVisible = false, List(), player))
    }

    "return the dog" in {
      val dog = Dog(List(TarotCard.CLUB_EIGHT, TarotCard.CLUB_FIVE))
      val winningBid = mock[Bid]
      val playerHands = List()
      val target = testKit.spawn(DogPhase(dog, winningBid, playerHands, mock[ScoreBoard]))
      val testProbe = testKit.createTestProbe[ViewedDog]
      val player = Player("Tester")

      when(winningBid.shouldDogBeVisible).thenReturn(true)
      when(winningBid.player).thenReturn(player)

      target ! ViewDog(testProbe.ref)

      testProbe.expectMessage(ViewedDog(isVisible = true, dog.cards, player))
    }
  }

  "For a given dog, dog phase receiving an update" should {
    "reject an update on hidden dog" in {
      val dog = mock[Dog]
      val player = Player("player1")
      val winningBid = mock[Bid]
      val playerHands = List()
      val target = testKit.spawn(DogPhase(dog, winningBid, playerHands, mock[ScoreBoard]))
      val testProbe = testKit.createTestProbe[UpdatedDog]

      when(winningBid.shouldDogBeVisible).thenReturn(false)
      when(winningBid.player).thenReturn(player)

      target ! UpdateDog(player, mock[Dog], testProbe.ref)

      testProbe.expectMessage(UpdatedDog(false))
    }

    "reject an update by wrong player" in {
      val dog = mock[Dog]
      val player = Player("player1")
      val winningBid = mock[Bid]
      val playerHands = List()
      val target = testKit.spawn(DogPhase(dog, winningBid, playerHands, mock[ScoreBoard]))
      val testProbe = testKit.createTestProbe[UpdatedDog]

      when(winningBid.player).thenReturn(Player("another player"))

      target ! UpdateDog(player, mock[Dog], testProbe.ref)

      testProbe.expectMessage(UpdatedDog(false))

      target ! GetPhaseName(phaseProbe.ref)
      phaseProbe.expectMessage(ReceivedPhaseName(PhaseName.DOG_PHASE))
    }

    "reject a dog with an unacceptable card" in {
      val dog = mock[Dog]
      val player = Player("player1")
      val winningBid = mock[Bid]
      val playerHands = List()
      val target = testKit.spawn(DogPhase(dog, winningBid, playerHands, mock[ScoreBoard]))
      val testProbe = testKit.createTestProbe[UpdatedDog]
      val newDog = Dog(List(TarotCard.HEART_KING))

      when(winningBid.shouldDogBeVisible).thenReturn(true)
      when(winningBid.player).thenReturn(player)

      target ! UpdateDog(player, newDog, testProbe.ref)

      testProbe.expectMessage(UpdatedDog(false))

      target ! GetPhaseName(phaseProbe.ref)
      phaseProbe.expectMessage(ReceivedPhaseName(PhaseName.DOG_PHASE))
    }

    "accepts a dog" in {
      val dog = Dog(List(TarotCard.CLUB_FIVE))
      val player = Player("player1")
      val winningBid = mock[Bid]
      val playerHands = List(Hand(player, List(TarotCard.CLUB_EIGHT)))
      val target = testKit.spawn(DogPhase(dog, winningBid, playerHands, mock[ScoreBoard]))
      val testProbe = testKit.createTestProbe[UpdatedDog]
      val newDog = Dog(List(TarotCard.CLUB_FIVE))

      when(winningBid.shouldDogBeVisible).thenReturn(true)
      when(winningBid.player).thenReturn(player)

      target ! UpdateDog(player, newDog, testProbe.ref)

      testProbe.expectMessage(UpdatedDog(true))

      target ! GetPhaseName(phaseProbe.ref)
      phaseProbe.expectMessage(ReceivedPhaseName(PhaseName.GAME_ROUND_PHASE))

    }
  }
}
