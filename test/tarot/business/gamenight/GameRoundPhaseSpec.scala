package tarot.business.gamenight

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import tarot.business.BiddingRound.{Bid, BidValue}
import tarot.business.Deck.Dog
import tarot.business.GreenMat.WinStack
import tarot.business.Player.{Hand, PlayerMove}
import tarot.business.TarotCard._
import tarot.business.gamenight.GameNight._
import tarot.business.{TarotCard => _, _}
import org.mockito.scalatest.MockitoSugar
import org.scalatest.BeforeAndAfterAll
import org.scalatestplus.play.PlaySpec

class GameRoundPhaseSpec extends PlaySpec with BeforeAndAfterAll with MockitoSugar {

  private val testKit = ActorTestKit()
  private val phaseProbe = testKit.createTestProbe[ReceivedPhaseName]

  override def afterAll(): Unit = testKit.shutdownTestKit()

  "In the game round phase, view cards" should {

    "return the list of cards on the green mat" in {
      val player1 = Player("player1")
      val winningBid = Bid(player1, BidValue.GARDE)
      val dog = mock[Dog]
      val greenMat = mock[GreenMat]
      val playerMoves = List()
      val gameTrick = GameTrick(List(), List(), greenMat)
      val testProbe = testKit.createTestProbe[GreenMatCards]
      val target = testKit.spawn(GameRoundPhase(winningBid, dog, player1, mock[ScoreBoard]).nextTrick(gameTrick))

      when(gameTrick.greenMatCards).thenReturn(playerMoves)

      target ! ViewGreenMatCards(testProbe.ref)

      testProbe.expectMessage(GreenMatCards(playerMoves))

      target ! GetPhaseName(phaseProbe.ref)
      phaseProbe.expectMessage(ReceivedPhaseName(PhaseName.GAME_ROUND_PHASE))
    }
  }

  "In the game round phase, moving a player" should {
    "fail basic validation" in {
      val player1 = Player("player1")
      val winningBid = Bid(player1, BidValue.GARDE)
      val dog = mock[Dog]
      val playerMove = PlayerMove(player1, CLUB_EIGHT)
      val gameTrick = mock[GameTrick]
      val testProbe = testKit.createTestProbe[MovedPlayer]
      val target = testKit.spawn(GameRoundPhase(winningBid, dog, player1, mock[ScoreBoard]).nextTrick(gameTrick))

      when(gameTrick.playerMove(playerMove)).thenReturn(Option.empty)
      when(gameTrick.greenMat).thenReturn(mock[GreenMat])
      when(gameTrick.playerHands).thenReturn(List())
      
      target ! MovePlayer(playerMove, testProbe.ref)

      val result = testProbe.expectMessageType[MovedPlayer]
      result.isMoveSuccessful mustBe false
      result.player mustBe player1

      target ! GetPhaseName(phaseProbe.ref)
      phaseProbe.expectMessage(ReceivedPhaseName(PhaseName.GAME_ROUND_PHASE))
    }

    "complete trick and move to setup phase" in {
      val players = List(Player("player1"), Player("player2"), Player("player3"))
      val winningBid = Bid(players.head, BidValue.GARDE)
      val dog = Dog(List(
        DIAMOND_KING, DIAMOND_EIGHT, DIAMOND_FIVE, DIAMOND_JACK, DIAMOND_ONE, DIAMOND_SIX
      ))
      val playerMove = PlayerMove(players.head, CLUB_EIGHT)
      val gameTrick = mock[GameTrick]
      val testProbe = testKit.createTestProbe[MovedPlayer]
      val scoreBoard = mock[ScoreBoard]
      val target = testKit.spawn(GameRoundPhase(winningBid, dog, players.head, scoreBoard).nextTrick(gameTrick))
      val nextTrick = mock[GameTrick]
      val playerHands = players.map(Hand(_, List()))

      when(gameTrick.nextBiddingRoundPlayers(players.head)).thenReturn(players)
      when(nextTrick.playerHands).thenReturn(playerHands)
      when(nextTrick.winStacks).thenReturn(List(WinStack(players.head, List(
        HEART_KING, HEART_EIGHT, HEART_FIVE,
        CLUB_KING, CLUB_EIGHT, CLUB_FIVE,
        SPADE_KING, SPADE_EIGHT, SPADE_FIVE,
      ))))
      when(nextTrick.greenMat).thenReturn(mock[GreenMat])
      when(gameTrick.playerMove(playerMove)).thenReturn(Option(nextTrick))
      when(scoreBoard.gameRoundScores).thenReturn(List())

      target ! MovePlayer(playerMove, testProbe.ref)

      val result = testProbe.expectMessageType[MovedPlayer]
      result.isMoveSuccessful mustBe true
      result.player mustBe players.head

      target ! GetPhaseName(phaseProbe.ref)
      phaseProbe.expectMessage(ReceivedPhaseName(PhaseName.BIDDING_PHASE))
    }

    "continue trick" in {
      val player1 = Player("player1")
      val winningBid = Bid(player1, BidValue.GARDE)
      val dog = mock[Dog]
      val playerMove = PlayerMove(player1, CLUB_EIGHT)
      val gameTrick = mock[GameTrick]
      val testProbe = testKit.createTestProbe[MovedPlayer]
      val target = testKit.spawn(GameRoundPhase(winningBid, dog, player1, mock[ScoreBoard]).nextTrick(gameTrick))
      val nextTrick = mock[GameTrick]
      val playerHands = List(Hand(player1, List(CLUB_EIGHT)))

      when(nextTrick.playerHands).thenReturn(playerHands)
      when(nextTrick.greenMat).thenReturn(mock[GreenMat])
      when(gameTrick.playerMove(playerMove)).thenReturn(Option(nextTrick))

      target ! MovePlayer(playerMove, testProbe.ref)

      val result = testProbe.expectMessageType[MovedPlayer]
      result.isMoveSuccessful mustBe true
      result.player mustBe player1

      target ! GetPhaseName(phaseProbe.ref)
      phaseProbe.expectMessage(ReceivedPhaseName(PhaseName.GAME_ROUND_PHASE))
    }

  }
}
