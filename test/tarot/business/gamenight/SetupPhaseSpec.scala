package tarot.business.gamenight

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import tarot.business.Deck.{DealtHands, Dog}
import tarot.business.Player.Hand
import tarot.business.gamenight.GameNight._
import tarot.business.{Deck, Player}
import org.mockito.scalatest.MockitoSugar
import org.scalatest.BeforeAndAfterAll
import org.scalatestplus.play.PlaySpec

class SetupPhaseSpec extends PlaySpec with BeforeAndAfterAll with MockitoSugar {

  private val testKit = ActorTestKit()

  override def afterAll(): Unit = testKit.shutdownTestKit()

  "Given a Deck, SetupPhase" should {
    val deck = mock[Deck]

    "return the phase name" in {
      val testProbe = testKit.createTestProbe[ReceivedPhaseName]
      val target = testKit.spawn(SetupPhase(deck))

      target ! GetPhaseName(testProbe.ref)

      testProbe.expectMessage(ReceivedPhaseName("Setup Phase"))
    }

    "accept to add a player" in {
      val testProbe = testKit.createTestProbe[AddedPlayer]
      val target = testKit.spawn(SetupPhase(deck))
      val player = Player("player")

      target ! AddPlayer(player, testProbe.ref)

      testProbe.expectMessage(AddedPlayer(true))
    }

    "return the empty list of players" in {
      val testProbe = testKit.createTestProbe[ViewedPlayers]
      val target = testKit.spawn(SetupPhase(deck))

      target ! ViewPlayers(testProbe.ref)

      val viewedPlayers = testProbe.expectMessageType[ViewedPlayers]
      viewedPlayers.isReadyToPlay mustBe false
      viewedPlayers.players.isEmpty mustBe true
    }

    "return the ready-to-play list of players" in {
      val testProbe = testKit.createTestProbe[ViewedPlayers]
      val addProbe = testKit.createTestProbe[AddedPlayer].ref
      val target = testKit.spawn(SetupPhase(deck))

      target ! AddPlayer(mock[Player], addProbe)
      target ! AddPlayer(mock[Player], addProbe)
      target ! AddPlayer(mock[Player], addProbe)
      target ! ViewPlayers(testProbe.ref)

      val viewedPlayers = testProbe.expectMessageType[ViewedPlayers]
      viewedPlayers.players.size mustBe 3
      viewedPlayers.isReadyToPlay mustBe true
    }
  }

  "Given three players, SetupPhase" should {

    "switch to bidding phase" in {
      val deck = mock[Deck]
      val addProbe = testKit.createTestProbe[AddedPlayer].ref
      val target = testKit.spawn(SetupPhase(deck))
      val testProbe = testKit.createTestProbe[StartedGame]
      val dealtHands = mock[DealtHands]
      val players = List(mock[Player], mock[Player], mock[Player])
      val phaseProbe = testKit.createTestProbe[ReceivedPhaseName]

      when(deck.distributeCards(anyList[Player])).thenReturn(dealtHands)
      when(dealtHands.playerHands).thenReturn(players.map(_ => mock[Hand]))
      when(dealtHands.dog).thenReturn(mock[Dog])

      players.foreach(target ! AddPlayer(_, addProbe))
      target ! StartGame(testProbe.ref)

      testProbe.expectMessage(StartedGame(true))
      target ! GetPhaseName(phaseProbe.ref)
      phaseProbe.expectMessage(ReceivedPhaseName("Bidding Phase"))
    }
  }

  "Given two players, SetupPhase" should {

    "not switch to bidding phase" in {
      val deck = mock[Deck]
      val addProbe = testKit.createTestProbe[AddedPlayer].ref
      val target = testKit.spawn(SetupPhase(deck))
      val testProbe = testKit.createTestProbe[StartedGame]

      target ! AddPlayer(mock[Player], addProbe)
      target ! AddPlayer(mock[Player], addProbe)
      target ! StartGame(testProbe.ref)

      testProbe.expectMessage(StartedGame(false))
    }
  }


}