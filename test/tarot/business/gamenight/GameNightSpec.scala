package tarot.business.gamenight

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import tarot.business.BiddingRound.BidValue
import tarot.business.Deck.Dog
import tarot.business.TarotCard._
import tarot.business.gamenight.GameNight._
import tarot.business.{Deck, Player, TarotCard}
import org.scalatest.BeforeAndAfterAll
import org.scalatestplus.play.PlaySpec

import scala.util.Random

class GameNightSpec extends PlaySpec with BeforeAndAfterAll {

  private val testKit = ActorTestKit()

  "Given a deck of cards, game night" should {
    "handle a full game" in {
      val random = getSameRandom
      val cards = random.shuffle(TarotCard.values.toList)
      val deck = Deck(cards, random)
      val gameNight = testKit.spawn(GameNight(deck))
      val players = List(Player("Madeleine"), Player("Viktor"), Player("Mathieu"))

      players.foreach(gameNight ! AddPlayer(_, testKit.createTestProbe[AddedPlayer].ref))

      gameNight ! StartGame(testKit.createTestProbe[StartedGame].ref)

      val playerHandsProbe = testKit.createTestProbe[ReceivedPlayerHand]
      players.foreach(gameNight ! GetPlayerHand(_, playerHandsProbe.ref))
      val playerHands = List(
        playerHandsProbe.expectMessageType[ReceivedPlayerHand].playerHand,
        playerHandsProbe.expectMessageType[ReceivedPlayerHand].playerHand,
        playerHandsProbe.expectMessageType[ReceivedPlayerHand].playerHand)

      playerHands.foreach(hand => {
        print(s"Player is ${hand.player}\n")
        hand.cards.sorted.foreach(card => {
          print(s"-- $card\n")
        })})

      gameNight ! PlaceBid(players.head, BidValue.PETITE, testKit.createTestProbe[PlacedBid].ref)
      gameNight ! PlaceBid(players(1), BidValue.GARDE, testKit.createTestProbe[PlacedBid].ref)
      gameNight ! PlaceBid(players.last, BidValue.PASS, testKit.createTestProbe[PlacedBid].ref)

      val viewDogProbe = testKit.createTestProbe[ViewedDog]
      gameNight ! ViewDog(viewDogProbe.ref)

      val dogCards = viewDogProbe.expectMessageType[ViewedDog].cards
      print("The dog is:\n")
      dogCards.sorted.foreach(card => print(s"-- $card\n"))

      val newDog = List(DIAMOND_SIX, CLUB_THREE, SPADE_TWO, SPADE_THREE, SPADE_KNIGHT, DIAMOND_QUEEN)

      gameNight ! UpdateDog(players(1), Dog(newDog), testKit.createTestProbe[UpdatedDog].ref)

    }
  }

  private def getSameRandom = {
    val random = Random

    random.setSeed(1)
    random
  }
}
