package tarot.business

import tarot.business.Player.{Hand, PlayerMove}
import tarot.business.TarotCard._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec

class GreenMatSpec extends PlaySpec with MockitoSugar {
  private val player1 = Player("1")
  private val player2 = Player("2")
  private val player3 = Player("3")

  "When the GreenMat is empty" should {

    "any first move is valid" in {
      val greenMat = GreenMat()
      val firstMove = PlayerMove(mock[Player], mock[TarotCard])
      val playerHand = Hand(firstMove.player, List(firstMove.card))

      val target = greenMat.validatePlayerMove(firstMove, playerHand)

      target mustBe true
    }
  }

  "When the requested suit is hearts, subsequent players" should {
    val playerCards = List(PlayerMove(player1, HEART_KING))
    val greenMat = GreenMat(playerCards)

    "follow suit when they can" in {
      val playerHand = Hand(player2, List(HEART_EIGHT, HEART_FIVE))
      val playerMove = PlayerMove(player2, HEART_FIVE)

      val target = greenMat.validatePlayerMove(playerMove, playerHand)

      target mustBe true
    }

    "must follow suit" in {
      val playerHand = Hand(player2, List(HEART_EIGHT))
      val playerMove = PlayerMove(player2, CLUB_EIGHT)

      val target = greenMat.validatePlayerMove(playerMove, playerHand)

      target mustBe false
    }

    "trump when they don't have the suit" in {
      val playerHand = Hand(player2, List(TRUMP_EIGHT, TRUMP_EIGHTEEN))
      val playerMove = PlayerMove(player2, TRUMP_EIGHTEEN)

      val target = greenMat.validatePlayerMove(playerMove, playerHand)

      target mustBe true
    }

    "must trump when they don't have the suit" in {
      val playerHand = Hand(player2, List(TRUMP_EIGHT))
      val playerMove = PlayerMove(player2, CLUB_EIGHT)

      val target = greenMat.validatePlayerMove(playerMove, playerHand)

      target mustBe false
    }

    "play anything else if they cannot trump" in {
      val playerHand = Hand(player2, List(CLUB_EIGHT, SPADE_EIGHT))
      val playerMove = PlayerMove(player2, SPADE_EIGHT)

      val target = greenMat.validatePlayerMove(playerMove, playerHand)

      target mustBe true
    }
  }

  "When there is trump on the green mat" should {
    val greenMat = GreenMat(List(PlayerMove(player1, TRUMP_FOUR),
      PlayerMove(player1, TRUMP_FOURTEEN),
      PlayerMove(player2, TRUMP_FIFTEEN),
      PlayerMove(player3, TRUMP_SIXTEEN),
    ))

    "play trump when they trump" in {

      val playerHand = Hand(player2, List(TRUMP_EIGHT, TRUMP_NINE))
      val playerMove = PlayerMove(player2, TRUMP_NINE)

      val target = greenMat.validatePlayerMove(playerMove, playerHand)

      target mustBe true
    }

    "must trump higher when they trump" in {

      val playerHand = Hand(player2, List(TRUMP_THREE, TRUMP_EIGHTEEN))
      val playerMove = PlayerMove(player2, TRUMP_THREE)

      val target = greenMat.validatePlayerMove(playerMove, playerHand)

      target mustBe false
    }

    "must trump lower when they trump and cannot go higher" in {

      val playerHand = Hand(player2, List(TRUMP_TWO, TRUMP_THREE))
      val playerMove = PlayerMove(player2, TRUMP_TWO)

      val target = greenMat.validatePlayerMove(playerMove, playerHand)

      target mustBe true
    }

  }

  "When a player finishes a trick without trumps" should {
    val playerMoves = List(PlayerMove(player1, HEART_FIVE), PlayerMove(player2, HEART_JACK))
    val greenMat = GreenMat(playerMoves, numberOfPlayers = 3)

    "follows suit and wins" in {
      val target = greenMat + PlayerMove(player3, HEART_KING)

      target.isTrickComplete mustBe true
      target.cardsPlayed.size mustBe 3
      target.winStacks.head.player mustBe player3
    }

    "follows suit and loses" in {
      val target = greenMat + PlayerMove(player3, HEART_TEN)

      target.isTrickComplete mustBe true
      target.winStacks.size mustBe 1
      target.winStacks.head.player mustBe player2
      target.winStacks.head.cards.size mustBe 3
    }

    "plays the joker" in {
      val target = greenMat + PlayerMove(player3, JOKER)

      target.isTrickComplete mustBe true
      target.winStacks.size mustBe 2
      target.winStacks.head.player mustBe player2
      target.winStacks.head.cards.size mustBe 2
    }

    "plays a trump" in {
      val target = greenMat + PlayerMove(player3, TRUMP_EIGHTEEN)

      target.isTrickComplete mustBe true
      target.winStacks.size mustBe 1
      target.winStacks.head.player mustBe player3
      target.winStacks.head.cards.size mustBe 3
    }
  }

  "When a player finishes a trick with trump" should {
    val playerMoves = List(PlayerMove(player1, HEART_FIVE), PlayerMove(player2, TRUMP_ELEVEN))
    val greenMat = GreenMat(playerMoves, numberOfPlayers = 3)

    "follows the suit" in {
      val target = greenMat + PlayerMove(player3, HEART_TEN)

      target.isTrickComplete mustBe true
      target.winStacks.size mustBe 1
      target.winStacks.head.player mustBe player2
      target.winStacks.head.cards.size mustBe 3
    }

    "trumps higher" in {
      val target = greenMat + PlayerMove(player3, TRUMP_EIGHTEEN)

      target.isTrickComplete mustBe true
      target.winStacks.size mustBe 1
      target.winStacks.head.player mustBe player3
      target.winStacks.head.cards.size mustBe 3
    }
  }
}
