package tarot.infrastructure

import java.util.concurrent.ConcurrentHashMap

import akka.actor.{ActorRef, ActorSystem}
import tarot.business.{Deck, TarotCard}
import javax.inject.{Inject, Singleton}

import scala.collection.mutable.ListBuffer
import scala.util.Random

@Singleton
class TarotSupervisor @Inject()(system: ActorSystem) {
  private final val games: ConcurrentHashMap[String, ActorRef] = new ConcurrentHashMap

  def getGameNight(gameId: String): ActorRef = {
    games.computeIfAbsent(gameId, _ => {
      val cards = Random.shuffle(TarotCard.values.toList)
      val deck = Deck(cards)

      system.actorOf(GameNightSupervisor.props(deck))
    })
  }

  def getGameIds: List[String] = {
    val gameKeys = games.keys()
    val gameIds = ListBuffer[String]()

    while (gameKeys.hasMoreElements) {
      gameIds += gameKeys.nextElement()
    }
    gameIds.toList
  }

}
