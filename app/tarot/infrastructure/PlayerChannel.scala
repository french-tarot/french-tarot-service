package tarot.infrastructure

import akka.actor.{Actor, ActorRef}
import tarot.business.Player
import tarot.infrastructure.GameNightSupervisor.{GameMessage, PLAYER_ACTION_KEY, PlayerAction, PlayerMessage}
import play.api.Logging
import play.api.libs.json.{JsValue, Json}


class PlayerChannel(websocket: ActorRef, gameNightSupervisor: ActorRef, player: Player) extends Actor with Logging {
  gameNightSupervisor ! PlayerMessage(player, Json.obj(PLAYER_ACTION_KEY -> PlayerAction.JOIN))

  def receive: Receive = {
    case payload: JsValue =>
      logger.debug(s"From ${player.name}, received message: $payload")
      gameNightSupervisor ! PlayerMessage(player, payload)
    case GameMessage(payload) =>
      logger.debug(s"To ${player.name}, sending message: $payload")
      websocket ! payload
  }
}
