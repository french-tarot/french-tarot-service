package tarot.infrastructure

import akka.actor.typed.scaladsl.adapter._
import akka.actor.{Actor, Props}
import tarot.business.BiddingRound.{Bid, BidValue}
import tarot.business.Deck.Dog
import tarot.business.Player.PlayerMove
import tarot.business.ScoreBoard.{GameRoundScore, PlayerScore}
import tarot.business.TarotCard.{TarotCard, TarotSuit}
import tarot.business.gamenight.GameNight
import tarot.business.gamenight.GameNight.{ViewPlayers => _, _}
import tarot.business.{Deck, Player, TarotCard}
import tarot.infrastructure.GameNightSupervisor.PlayerAction.PlayerAction
import tarot.infrastructure.GameNightSupervisor._
import play.api.Logging
import play.api.libs.json._

class GameNightSupervisor(deck: Deck) extends Actor with Logging {
  private val gameNight = context.spawnAnonymous(GameNight(deck))
  implicit val playerActionReader: Reads[PlayerAction] = Reads.enumNameReads(PlayerAction)
  implicit val playerWriter: OWrites[Player] = Json.writes[Player]
  implicit val tarotCardWriter: Writes[TarotCard] = (tarotCard: TarotCard) =>
    Json.obj("card-name" -> tarotCard.toString, "card-value" -> tarotCard.points)
  implicit val playerMoveWriter: OWrites[PlayerMove] = Json.writes[PlayerMove]
  implicit val bidWriter: OWrites[Bid] = Json.writes[Bid]
  implicit val playerScoreWriter: OWrites[PlayerScore] = Json.writes[PlayerScore]
  implicit val gameRoundScoreWriter: OWrites[GameRoundScore] = Json.writes[GameRoundScore]

  override def receive: Receive = start(Map())

  private def start(players: Map[Player, akka.actor.ActorRef]): Receive = {
    case _: ViewPlayers =>
      val playerList = players.toList
      val playerNames = playerList.map(_._1).map(_.name).mkString(", ")
      val message = s"Ready to play with: $playerNames"

      sender() ! GameMessage(Json.obj(GAME_EVENT_KEY -> GameEvent.INFO_MESSAGE, GAME_MESSAGE_KEY -> message))
    case PlayerMessage(player, payload) =>
      val action = (payload \ PLAYER_ACTION_KEY).asOpt[PlayerAction]

      if (action.contains(PlayerAction.JOIN)) {
        if (players.contains(player)) {
          logger.info(s"Reconnecting player ${player.name}")
          gameNight ! GetPlayerHand(player, self)
          gameNight ! AskWhoIsNext(self)
        } else {
          logger.info(s"Adding player ${player.name}")
          gameNight ! AddPlayer(player, self)
        }
        context become start(players ++ Map(player -> sender()))
      } else if (action.isDefined) {
        handle(action.get, player, players, payload)
      } else {
        sender() ! GameMessage(Json.obj(
          GAME_EVENT_KEY -> GameEvent.INFO_MESSAGE,
          GAME_MESSAGE_KEY -> "Unrecognized player command"))
      }
    case AddedPlayer(isPlayerAdded) =>
      if (isPlayerAdded) {
        val playerList = players.toList
        val playerNames = playerList.map(_._1).map(_.name).mkString(", ")
        val message = s"Ready to play with: $playerNames"

        broadCastInfoMessage(message, players)
      }
    case ReceivedPlayerHand(playerHand) =>
      players.get(playerHand.player).foreach(playerChannel =>
        playerChannel ! GameMessage(Json.obj(
          GAME_EVENT_KEY -> GameEvent.PLAYER_CARDS_VIEW,
          "cards" -> playerHand.cards
        )))
    case StartedGame(isGameStarted) =>
      broadCastInfoMessage(if (isGameStarted) "Game started" else "Game failed to start", players)
      gameNight ! AskWhoIsNext(self)
    case NextPlayer(player, phaseName) =>
      players.toList.map(_._2).foreach(_ ! GameMessage(Json.obj(
        GAME_EVENT_KEY -> GameEvent.NEXT_PLAYER,
        "player" -> Json.toJson(player),
        "phase" -> phaseName
      )))
      if (phaseName == PhaseName.DOG_PHASE) {
        gameNight ! ViewDog(self)
      }
    case PlacedBid(player, bidValue, isBidValid) =>
      if (isBidValid) {
        broadCastInfoMessage(s"Player $player placed a bid $bidValue", players)
      } else {
        tellPlayer(player, players, Map(
          GAME_EVENT_KEY -> GameEvent.INFO_MESSAGE.toString,
          GAME_MESSAGE_KEY -> "Invalid bid"))
      }
      gameNight ! AskWhoIsNext(self)
    case ReceivedPhaseName(phaseName) =>
      broadCastInfoMessage(s"$phaseName starts", players)
      phaseName match {
        case PhaseName.GAME_ROUND_PHASE => gameNight ! AskWhoIsNext(self)
        case PhaseName.PARTNER_UP_PHASE => gameNight ! AskWhoWonBidding(self)
      }
    case BidWinnerCallingPartner(player) =>
      tellPlayer(player, players, Map(
        GAME_EVENT_KEY -> GameEvent.CALL_KING.toString,
        GAME_MESSAGE_KEY -> "Please pick a suit to call a King"))
    case ChosePartnerSuit(isValid, player, suit) =>
      if (isValid) {
        broadCastInfoMessage(s"$player is calling the King of ${suit.toString}", players)
        gameNight ! AskWhoIsNext(self)
      } else {
        tellPlayer(player, players, Map(
          GAME_EVENT_KEY -> GameEvent.CALL_KING.toString,
          GAME_MESSAGE_KEY -> "Please pick a suit to call a King"))
      }
    case MovedPlayer(isMoveSuccessful, player, greenMat) =>
      if (isMoveSuccessful) {
        players.toList.map(_._2).foreach(_ ! GameMessage(Json.obj(
          GAME_EVENT_KEY -> GameEvent.GREEN_MAT_CARDS,
          "playerMoves" -> Json.toJson(greenMat.cardsPlayed))))
        gameNight ! AskWhoIsNext(self)

      } else {
        broadCastInfoMessage(s"$player made an invalid move", players)
        gameNight ! AskWhoIsNext(self)
      }
    case GreenMatCards(cards) =>
      players.toList.map(_._2).foreach(_ ! GameMessage(Json.obj(
        GAME_EVENT_KEY -> GameEvent.GREEN_MAT_CARDS,
        "playerMoves" -> Json.toJson(cards))))

      gameNight ! AskWhoIsNext(self)
    case UpdatedDog(isDogUpdated) =>
      if (isDogUpdated) {
        broadCastInfoMessage("Dog successfully updated", players)
        askPhaseName()
      }
    case RetrievedScoreBoard(player, scoreBoard) =>
      players.get(player).foreach(_ ! GameMessage(Json.obj(
        GAME_EVENT_KEY -> GameEvent.SCORE_BOARD,
        "roundScores" -> Json.toJson(scoreBoard.gameRoundScores)
      )))
    case ViewedDog(isVisible, cards, bidWinner) =>
      val verb = if (isVisible) "is" else "is not"

      broadCastInfoMessage(s"$bidWinner won the bidding. The dog $verb visible", players)
      players.toList.map(_._2).foreach(_ ! GameMessage(Json.obj(
        GAME_EVENT_KEY -> GameEvent.DOG_VIEW,
        "bidWinner" -> bidWinner,
        "isDogVisible" -> isVisible,
        "cards" -> Json.toJson(cards))))
  }

  private def askPhaseName(): Unit = gameNight ! GetPhaseName(self)

  private def tellPlayer(player: Player,
                         players: Map[Player, akka.actor.ActorRef],
                         message: Map[String, String]): Unit = {
    players.get(player).foreach(_ ! GameMessage(Json.toJson(message)))
  }

  private def broadCastInfoMessage(message: String, players: Map[Player, akka.actor.ActorRef]): Unit = {
    players.toList.map(_._2).foreach(_ ! GameMessage(Json.obj(
      GAME_EVENT_KEY -> GameEvent.INFO_MESSAGE,
      GAME_MESSAGE_KEY -> message)))
  }

  private def handle(action: PlayerAction, player: Player,
                     players: Map[Player, akka.actor.ActorRef], payload: JsValue): Unit = {
    logger.info(s"Handling action $action for player ${player.name}")
    action match {
      case PlayerAction.START_GAME =>
        gameNight ! StartGame(self)
        players.foreach(playerChannel => gameNight ! GetPlayerHand(playerChannel._1, self))
      case PlayerAction.VIEW_HAND =>
        gameNight ! GetPlayerHand(player, self)
      case PlayerAction.BID =>
        val bidValue = (payload \ "bid-value").asOpt[String]

        bidValue.foreach(bid => gameNight ! PlaceBid(player, BidValue.withName(bid), self))
      case PlayerAction.CALL_KING =>
        val kingSuit = (payload \ "suit").asOpt[String]

        kingSuit.map(TarotSuit.withName).foreach(gameNight ! ChoosePartnerSuit(player, _, self))
      case PlayerAction.UPDATE_DOG =>
        val proposedDog = (payload \ "cards").asOpt[List[String]]

        proposedDog.map(cards => cards.map(TarotCard.withName)).map(cards => Dog(cards))
          .foreach(newDog => gameNight ! UpdateDog(player, newDog, self))
      case PlayerAction.GET_SCORES => gameNight ! GetScoreBoard(player, self)
      case PlayerAction.MOVE =>
        val proposedMove = (payload \ "card").asOpt[String]

        proposedMove.map(TarotCard.withName).map(PlayerMove(player, _)).foreach(gameNight ! MovePlayer(_, self))
      case _ =>
        players.foreach(playerChannel =>
          playerChannel._2 ! GameMessage(Json.obj(
            GAME_EVENT_KEY -> GameEvent.INFO_MESSAGE,
            GAME_MESSAGE_KEY -> "Unhandled command")))
    }
  }
}

object GameNightSupervisor {

  def props(deck: Deck): Props = {
    Props(new GameNightSupervisor(deck))
  }

  case class GameMessage(payload: JsValue)

  case class PlayerMessage(player: Player, payload: JsValue)

  case class ViewPlayers()

  object PlayerAction extends Enumeration {
    type PlayerAction = Value

    val JOIN: GameNightSupervisor.PlayerAction.Value = Value("join")
    val START_GAME: GameNightSupervisor.PlayerAction.Value = Value("start-game")
    val BID: GameNightSupervisor.PlayerAction.Value = Value("bid")
    val CALL_KING: GameNightSupervisor.PlayerAction.Value = Value("call-king")
    val UPDATE_DOG: GameNightSupervisor.PlayerAction.Value = Value("update-dog")
    val MOVE: GameNightSupervisor.PlayerAction.Value = Value("move")
    val VIEW_HAND: GameNightSupervisor.PlayerAction.Value = Value("view-hand")
    val GET_SCORES: GameNightSupervisor.PlayerAction.Value = Value("get-scores")
  }

  object GameEvent extends Enumeration {
    type GameEvent = Value

    val PLAYER_CARDS_VIEW: GameNightSupervisor.GameEvent.Value = Value("player-cards-view")
    val INFO_MESSAGE: GameNightSupervisor.GameEvent.Value = Value("info-message")
    val NEXT_PLAYER: GameNightSupervisor.GameEvent.Value = Value("next-player")
    val DOG_VIEW: GameNightSupervisor.GameEvent.Value = Value("dog-view")
    val CALL_KING: GameNightSupervisor.GameEvent.Value = Value("call-king")
    val GREEN_MAT_CARDS: GameNightSupervisor.GameEvent.Value = Value("green-mat-cards")
    val SCORE_BOARD: GameNightSupervisor.GameEvent.Value = Value("score-board")
  }

  val PLAYER_ACTION_KEY = "action"
  val GAME_EVENT_KEY = "event"
  val GAME_MESSAGE_KEY = "message"

}