package tarot.infrastructure

import akka.actor.{ActorRef, Props}
import tarot.business.Player
import javax.inject.{Inject, Singleton}
import play.api.Logging

@Singleton
class PlayerSupervisor @Inject()(tarotSupervisor: TarotSupervisor) extends Logging {

  def props(websocket: ActorRef, gameId: String, player: Player): Props = {
    logger.info("New socket created")
    val gameNightSupervisor = tarotSupervisor.getGameNight(gameId)

    Props(new PlayerChannel(websocket, gameNightSupervisor, player))
  }

}
