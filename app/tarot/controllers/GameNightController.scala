package tarot.controllers

import akka.actor.ActorRef
import akka.util.Timeout
import tarot.infrastructure.GameNightSupervisor.{GameMessage, ViewPlayers}
import tarot.infrastructure.TarotSupervisor
import javax.inject.Inject
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class GameNightController @Inject()(tarotSupervisor: TarotSupervisor,
                                    implicit val scheduler: akka.actor.typed.Scheduler,
                                    implicit val ec: ExecutionContext,
                                    val controllerComponents: ControllerComponents)
  extends BaseController {
  private implicit val timeout: Timeout = Timeout(30.seconds)

  def index(): Action[AnyContent] = Action { _ =>
    val gameIds = tarotSupervisor.getGameIds

    Ok(Json.toJson(gameIds))
  }

  def showGame(gameId: String): Action[AnyContent] = Action.async { _ =>
    val gameNight: ActorRef = tarotSupervisor.getGameNight(gameId)
    import akka.pattern.ask

    import scala.concurrent.duration._
    implicit val timeout: Timeout = 5.seconds

    (gameNight ? ViewPlayers()).mapTo[GameMessage].map(gameMessage => Ok(gameMessage.payload))
  }

}
