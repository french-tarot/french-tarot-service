package tarot.controllers

import akka.actor.ActorSystem
import akka.stream.Materializer
import tarot.business.Player
import tarot.infrastructure.PlayerSupervisor
import javax.inject.Inject
import play.api.libs.json.JsValue
import play.api.libs.streams.ActorFlow
import play.api.mvc.{AbstractController, ControllerComponents, WebSocket}


class WebSocketController @Inject()(cc: ControllerComponents, playerSupervisor: PlayerSupervisor)
                                   (implicit system: ActorSystem, mat: Materializer)
  extends AbstractController(cc) {

  def socket(gameId: String, playerName: String): WebSocket = WebSocket.accept[JsValue, JsValue] { _ =>
    ActorFlow.actorRef { out =>
      playerSupervisor.props(out, gameId, Player(playerName))
    }
  }

}
