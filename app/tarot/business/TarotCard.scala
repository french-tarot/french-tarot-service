package tarot.business

import scala.util.Random


object TarotCard extends Enumeration {

  object TarotSuit extends Enumeration {
    type TarotSuit = Value
    val TRUMP, HEART, SPADE, CLUB, DIAMOND, JOKER = Value
  }

  // All cards
  import tarot.business.TarotCard.TarotSuit._

  import scala.language.implicitConversions

  implicit def valueToVal(x: Value): Val = x.asInstanceOf[Val]

  protected case class Val(rank: Int, points: Double, suit: TarotSuit) extends super.Val {
    def isTrump: Boolean = suit == TarotSuit.TRUMP
    def isNotTrump: Boolean = !isTrump
    def isJoker: Boolean = suit == TarotSuit.JOKER
    def isNotJoker: Boolean = !isJoker

    def isBout: Boolean = {
      this == TRUMP_TWENTY_ONE || this == TRUMP_ONE || this == JOKER
    }

  }
  type TarotCard = Value

  def getNewDeck: List[TarotCard] = {
    Random.shuffle(TarotCard.values.toList)
  }

  val JOKER: Val = Val(0, 4.5, TarotSuit.JOKER)

  val TRUMP_ONE: Val = Val(1, 4.5, TRUMP)
  val TRUMP_TWO: Val = Val(2, 0.5, TRUMP)
  val TRUMP_THREE: Val = Val(3, 0.5, TRUMP)
  val TRUMP_FOUR: Val = Val(4, 0.5, TRUMP)
  val TRUMP_FIVE: Val = Val(5, 0.5, TRUMP)
  val TRUMP_SIX: Val = Val(6, 0.5, TRUMP)
  val TRUMP_SEVEN: Val = Val(7, 0.5, TRUMP)
  val TRUMP_EIGHT: Val = Val(8, 0.5, TRUMP)
  val TRUMP_NINE: Val = Val(9, 0.5, TRUMP)
  val TRUMP_TEN: Val = Val(10, 0.5, TRUMP)
  val TRUMP_ELEVEN: Val = Val(11, 0.5, TRUMP)
  val TRUMP_TWELVE: Val = Val(12, 0.5, TRUMP)
  val TRUMP_THIRTEEN: Val = Val(13, 0.5, TRUMP)
  val TRUMP_FOURTEEN: Val = Val(14, 0.5, TRUMP)
  val TRUMP_FIFTEEN: Val = Val(15, 0.5, TRUMP)
  val TRUMP_SIXTEEN: Val = Val(16, 0.5, TRUMP)
  val TRUMP_SEVENTEEN: Val = Val(17, 0.5, TRUMP)
  val TRUMP_EIGHTEEN: Val = Val(18, 0.5, TRUMP)
  val TRUMP_NINETEEN: Val = Val(19, 0.5, TRUMP)
  val TRUMP_TWENTY: Val = Val(20, 0.5, TRUMP)
  val TRUMP_TWENTY_ONE: Val = Val(21, 4.5, TRUMP)

  val HEART_ONE: Val = Val(1, 0.5, HEART)
  val HEART_TWO: Val = Val(2, 0.5, HEART)
  val HEART_THREE: Val = Val(3, 0.5, HEART)
  val HEART_FOUR: Val = Val(4, 0.5, HEART)
  val HEART_FIVE: Val = Val(5, 0.5, HEART)
  val HEART_SIX: Val = Val(6, 0.5, HEART)
  val HEART_SEVEN: Val = Val(7, 0.5, HEART)
  val HEART_EIGHT: Val = Val(8, 0.5, HEART)
  val HEART_NINE: Val = Val(9, 0.5, HEART)
  val HEART_TEN: Val = Val(10, 0.5, HEART)
  val HEART_JACK: Val = Val(11, 1.5, HEART)
  val HEART_KNIGHT: Val = Val(12, 2.5, HEART)
  val HEART_QUEEN: Val = Val(13, 3.5, HEART)
  val HEART_KING: Val = Val(14, 4.5, HEART)

  val DIAMOND_ONE: Val = Val(1, 0.5, DIAMOND)
  val DIAMOND_TWO: Val = Val(2, 0.5, DIAMOND)
  val DIAMOND_THREE: Val = Val(3, 0.5, DIAMOND)
  val DIAMOND_FOUR: Val = Val(4, 0.5, DIAMOND)
  val DIAMOND_FIVE: Val = Val(5, 0.5, DIAMOND)
  val DIAMOND_SIX: Val = Val(6, 0.5, DIAMOND)
  val DIAMOND_SEVEN: Val = Val(7, 0.5, DIAMOND)
  val DIAMOND_EIGHT: Val = Val(8, 0.5, DIAMOND)
  val DIAMOND_NINE: Val = Val(9, 0.5, DIAMOND)
  val DIAMOND_TEN: Val = Val(10, 0.5, DIAMOND)
  val DIAMOND_JACK: Val = Val(11, 1.5, DIAMOND)
  val DIAMOND_KNIGHT: Val = Val(12, 2.5, DIAMOND)
  val DIAMOND_QUEEN: Val = Val(13, 3.5, DIAMOND)
  val DIAMOND_KING: Val = Val(14, 4.5, DIAMOND)

  val CLUB_ONE: Val = Val(1, 0.5, CLUB)
  val CLUB_TWO: Val = Val(2, 0.5, CLUB)
  val CLUB_THREE: Val = Val(3, 0.5, CLUB)
  val CLUB_FOUR: Val = Val(4, 0.5, CLUB)
  val CLUB_FIVE: Val = Val(5, 0.5, CLUB)
  val CLUB_SIX: Val = Val(6, 0.5, CLUB)
  val CLUB_SEVEN: Val = Val(7, 0.5, CLUB)
  val CLUB_EIGHT: Val = Val(8, 0.5, CLUB)
  val CLUB_NINE: Val = Val(9, 0.5, CLUB)
  val CLUB_TEN: Val = Val(10, 0.5, CLUB)
  val CLUB_JACK: Val = Val(11, 1.5, CLUB)
  val CLUB_KNIGHT: Val = Val(12, 2.5, CLUB)
  val CLUB_QUEEN: Val = Val(13, 3.5, CLUB)
  val CLUB_KING: Val = Val(14, 4.5, CLUB)

  val SPADE_ONE: Val = Val(1, 0.5, SPADE)
  val SPADE_TWO: Val = Val(2, 0.5, SPADE)
  val SPADE_THREE: Val = Val(3, 0.5, SPADE)
  val SPADE_FOUR: Val = Val(4, 0.5, SPADE)
  val SPADE_FIVE: Val = Val(5, 0.5, SPADE)
  val SPADE_SIX: Val = Val(6, 0.5, SPADE)
  val SPADE_SEVEN: Val = Val(7, 0.5, SPADE)
  val SPADE_EIGHT: Val = Val(8, 0.5, SPADE)
  val SPADE_NINE: Val = Val(9, 0.5, SPADE)
  val SPADE_TEN: Val = Val(10, 0.5, SPADE)
  val SPADE_JACK: Val = Val(11, 1.5, SPADE)
  val SPADE_KNIGHT: Val = Val(12, 2.5, SPADE)
  val SPADE_QUEEN: Val = Val(13, 3.5, SPADE)
  val SPADE_KING: Val = Val(14, 4.5, SPADE)
}
