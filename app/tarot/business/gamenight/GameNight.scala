package tarot.business.gamenight

import akka.actor.typed.{ActorRef, Behavior}
import tarot.business.BiddingRound.BidValue.BidValue
import tarot.business.Deck.Dog
import tarot.business.Player.{Hand, PlayerMove}
import tarot.business.TarotCard.TarotCard
import tarot.business.TarotCard.TarotSuit.TarotSuit
import tarot.business._

object GameNight {

  sealed trait Command

  def apply(deck: Deck): Behavior[Command] = SetupPhase(deck)

  final case class GetPhaseName(replyTo: ActorRef[ReceivedPhaseName]) extends Command

  final case class ReceivedPhaseName(phaseName: String)

  object PhaseName extends Enumeration {
    type PhaseName = Value
    val SETUP_PHASE = "Setup Phase"
    val BIDDING_PHASE = "Bidding Phase"
    val GAME_ROUND_PHASE = "Game Round Phase"
    val DOG_PHASE = "Dog Phase"
    val PARTNER_UP_PHASE = "Partner Up Phase"
  }

  // Setup Phase
  final case class AddPlayer(player: Player, replyTo: ActorRef[AddedPlayer]) extends Command

  final case class StartGame(replyTo: ActorRef[StartedGame]) extends Command

  final case class ViewPlayers(replyTo: ActorRef[ViewedPlayers]) extends Command

  final case class ViewedPlayers(players: List[Player], isReadyToPlay: Boolean)

  final case class AddedPlayer(isPlayerAdded: Boolean)

  final case class StartedGame(isGameStarted: Boolean)

  // Bidding phase
  final case class PlaceBid(player: Player, bidValue: BidValue, replyTo: ActorRef[PlacedBid]) extends Command

  final case class PlacedBid(player: Player, bidValue: BidValue, isBidValid: Boolean)

  final case class GetPlayerHand(player: Player, replyTo: ActorRef[ReceivedPlayerHand]) extends Command

  final case class ReceivedPlayerHand(playerHand: Hand)

  final case class AskWhoIsNext(replyTo: ActorRef[NextPlayer]) extends Command

  final case class NextPlayer(player: Player, phaseName: String)

  // Partner up phase
  final case class AskWhoWonBidding(replyTo: ActorRef[BidWinnerCallingPartner]) extends Command
  final case class ChoosePartnerSuit(leader: Player, suit: TarotSuit, replyTo: ActorRef[ChosePartnerSuit])
    extends Command

  final case class ChosePartnerSuit(isValid: Boolean, player: Player, suit: TarotSuit)
  final case class BidWinnerCallingPartner(player: Player)

  // Dog phase
  final case class ViewDog(replyTo: ActorRef[ViewedDog]) extends Command

  final case class ViewedDog(isVisible: Boolean, cards: List[TarotCard], bidWinner: Player)

  final case class UpdateDog(player: Player, newDog: Dog, replyTo: ActorRef[UpdatedDog]) extends Command

  final case class UpdatedDog(isDogUpdated: Boolean)


  // Game round phase
  final case class MovePlayer(playerMove: PlayerMove, replyTo: ActorRef[MovedPlayer]) extends Command

  final case class MovedPlayer(isMoveSuccessful: Boolean, player: Player, greenMat: GreenMat)

  final case class ViewGreenMatCards(replyTo: ActorRef[GreenMatCards]) extends Command

  final case class GreenMatCards(playerMoves: List[PlayerMove])

  // Score
  final case class GetScoreBoard(player: Player, replyTo: ActorRef[RetrievedScoreBoard]) extends Command

  final case class RetrievedScoreBoard(player: Player, scoreBoard: ScoreBoard)

}
