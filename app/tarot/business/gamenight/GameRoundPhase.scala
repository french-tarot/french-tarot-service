package tarot.business.gamenight

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import tarot.business.BiddingRound.Bid
import tarot.business.Deck.Dog
import tarot.business.Player.Hand
import tarot.business.gamenight.GameNight._
import tarot.business._
import play.api.Logging

case class GameRoundPhase(winningBid: Bid, dog: Dog, starter: Player, scoreBoard: ScoreBoard) extends Logging {
  logger.info(s"Game Round Phase: $winningBid")

  def nextTrick(gameTrick: GameTrick): Behavior[Command] =
    Behaviors.receiveMessage {
      case GetPlayerHand(player, replyTo) =>
        val playerHand = gameTrick.playerHands.find(_.player == player).getOrElse(Hand(player, List()))

        replyTo ! ReceivedPlayerHand(playerHand)
        Behaviors.same
      case MovePlayer(playerMove, replyTo) =>
        logger.debug(s"Player moves: $playerMove")

        val player = playerMove.player
        val nextGameTrick = gameTrick.playerMove(playerMove)

        if (nextGameTrick.isDefined) {
          val nextGame = nextGameTrick.get

          if (nextGame.playerHands.head.cards.isEmpty) {
            logger.debug(f"$playerMove completes the round")

            val deck = Deck(nextGame.winStacks.flatMap(_.cards) ++ dog.cards)
            val orderedPlayers = gameTrick.nextBiddingRoundPlayers(starter)
            val newScoreBoard = ScoreCalculator(nextGame.winStacks, dog, winningBid, orderedPlayers, scoreBoard)

            replyTo ! MovedPlayer(isMoveSuccessful = true, player, nextGame.greenMat)
            BiddingPhase.switchToBiddingPhase(deck, orderedPlayers, newScoreBoard)
          } else {
            logger.debug(f"$playerMove successful, continuing to next player")

            replyTo ! MovedPlayer(isMoveSuccessful = true, player, nextGame.greenMat)
            nextTrick(nextGame)
          }
        } else {
          logger.warn(
            f"$playerMove is invalid for cards ${gameTrick.playerHands.filter(_.player == playerMove.player)}")

          replyTo ! MovedPlayer(isMoveSuccessful = false, player, gameTrick.greenMat)
          Behaviors.same
        }
      case ViewGreenMatCards(replyTo) =>
        logger.debug(s"ViewGreenMatCards: ${gameTrick.greenMatCards}")

        replyTo ! GreenMatCards(gameTrick.greenMatCards)
        Behaviors.same
      case AskWhoIsNext(replyTo) =>
        replyTo ! NextPlayer(gameTrick.nextPlayer, PhaseName.GAME_ROUND_PHASE)
        Behaviors.same
      case GetPhaseName(replyTo) =>
        replyTo ! ReceivedPhaseName(PhaseName.GAME_ROUND_PHASE)
        Behaviors.same
      case _ =>
        logger.error("Unhandled message")
        Behaviors.unhandled
    }
}

