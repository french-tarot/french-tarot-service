package tarot.business.gamenight

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import tarot.business.gamenight.GameNight._
import tarot.business.{BiddingRound, Deck, Player, ScoreBoard}
import play.api.Logging

object SetupPhase extends Logging {
  logger.info("Setup Phase starts")

  def apply(deck: Deck, players: List[Player] = List(), scoreBoard: ScoreBoard = ScoreBoard()): Behavior[Command] =
    Behaviors.receiveMessage {
      case AddPlayer(player, replyTo) =>
        logger.info(s"Adding player $player")
        if (players.contains(player)) {
          replyTo ! AddedPlayer(true)
          Behaviors.same
        } else if (players.size < maxNumberOfPlayers) {
          replyTo ! AddedPlayer(true)
          apply(deck, players ++ List(player))
        } else {
          replyTo ! AddedPlayer(false)
          Behaviors.same
        }
      case ViewPlayers(replyTo) =>
        logger.info("Viewing list of players")
        replyTo ! ViewedPlayers(players, numberOfPlayersSupported.contains(players.size))
        Behaviors.same
      case StartGame(replyTo) =>
        logger.info("Starting game...")
        if (numberOfPlayersSupported.contains(players.size)) {
          replyTo ! StartedGame(true)
          BiddingPhase.switchToBiddingPhase(deck, players, scoreBoard)
        } else {
          replyTo ! StartedGame(false)
          Behaviors.same
        }
      case GetPhaseName(replyTo) =>
        replyTo ! ReceivedPhaseName(PhaseName.SETUP_PHASE)
        Behaviors.same
      case _ =>
        logger.error("Unhandled message")
        Behaviors.unhandled
    }

  private val numberOfPlayersSupported = Set(3, 4, 5)
  private val maxNumberOfPlayers = numberOfPlayersSupported.max

}
