package tarot.business.gamenight

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import tarot.business.BiddingRound.Bid
import tarot.business.Deck.Dog
import tarot.business.Player.Hand
import tarot.business.gamenight.GameNight._
import tarot.business.{BiddingRound, GameTrick, GreenMat, ScoreBoard}
import play.api.Logging

object PartnerUpPhase extends Logging {
  private val PARTNER_PARTY_SIZE = 5

  def apply(biddingRound: BiddingRound, dog: Dog, scoreBoard: ScoreBoard): Behavior[Command] = {
    val playerHands = biddingRound.playerHands
    val winningBid = biddingRound.getWinningBid

    if (playerHands.size == PARTNER_PARTY_SIZE && winningBid.isDefined) {
      logger.info(s"Starting partner-up phase for ${biddingRound.getWinningBid}")
      partnerUpPhase(playerHands, winningBid.get, dog, scoreBoard)
    } else {
      logger.info("Not enough players to partner-up")
      nextPhase(playerHands, winningBid, dog, scoreBoard)
    }
  }

  private def partnerUpPhase(playerHands: List[Hand], winningBid: Bid,
                             dog: Dog, scoreBoard: ScoreBoard): Behavior[Command] =
    Behaviors.receiveMessage {
      case AskWhoIsNext(replyTo) =>
        replyTo ! NextPlayer(winningBid.player, PhaseName.PARTNER_UP_PHASE)
        Behaviors.same
      case AskWhoWonBidding(replyTo) =>
        replyTo ! BidWinnerCallingPartner(winningBid.player)
        Behaviors.same
      case ChoosePartnerSuit(leader, suit, replyTo) =>
        if (leader != winningBid.player) {
          replyTo ! ChosePartnerSuit(isValid = false, leader, suit)
          Behaviors.same
        } else {
          val newBid = winningBid.withPartner(suit, playerHands)

          logger.debug(s"Suit called is $suit. Winning bid is $newBid.")
          replyTo ! ChosePartnerSuit(isValid = true, leader, suit)
          nextPhase(playerHands, Option(newBid), dog, scoreBoard)
        }
      case GetPhaseName(replyTo) =>
        replyTo ! ReceivedPhaseName(PhaseName.PARTNER_UP_PHASE)
        Behaviors.same
      case _ =>
        logger.error("Unhandled message")
        Behaviors.unhandled
    }

  private def nextPhase(playerHands: List[Hand], winningBid: Option[Bid], dog: Dog,
                        scoreBoard: ScoreBoard): Behavior[Command] = {

    if (winningBid.map(_.shouldDogBeVisible).exists(identity)) {
      DogPhase(dog, winningBid.get, playerHands, scoreBoard)
    } else {
      val greenMat = GreenMat(numberOfPlayers = playerHands.size)
      val gameTrick = GameTrick(playerHands, List(), greenMat)

      GameRoundPhase(winningBid.get, dog, playerHands.head.player, scoreBoard).nextTrick(gameTrick)
    }
  }

}
