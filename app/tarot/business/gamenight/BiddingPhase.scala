package tarot.business.gamenight

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import tarot.business.Deck.Dog
import tarot.business.Player.Hand
import tarot.business.gamenight.GameNight._
import tarot.business.{BiddingRound, Deck, GameTrick, Player, ScoreBoard}
import play.api.Logging

case class BiddingPhase(dog: Dog, scoreBoard: ScoreBoard) extends Logging {
  logger.info(s"Bidding Phase starts with hidden $dog")

  def nextBid(biddingRound: BiddingRound): Behavior[Command] = Behaviors.receiveMessage {
    case GetScoreBoard(player, replyTo) =>
      replyTo ! RetrievedScoreBoard(player, scoreBoard)
      Behaviors.same
    case GetPlayerHand(player, replyTo) =>
      val playerHand = biddingRound.playerHands.find(_.player == player).getOrElse(Hand(player, List()))

      replyTo ! ReceivedPlayerHand(playerHand)
      Behaviors.same
    case PlaceBid(player, bidValue, replyTo) =>
      val nextRound = biddingRound.placeNextBid(player, bidValue)

      if (nextRound.isEmpty) {
        replyTo ! PlacedBid(player, bidValue, isBidValid = false)
        Behaviors.same
      } else {
        val newBiddingRound = nextRound.get

        if (newBiddingRound.isBiddingFinished) {
          replyTo ! PlacedBid(player, bidValue, isBidValid = true)

          if (newBiddingRound.didEveryonePass) {
            val cards = newBiddingRound.playerHands.flatMap(_.cards) ++ dog.cards
            val players = newBiddingRound.playerHands.map(_.player)
            val nextPlayers = GameTrick.rotateListPlayer(players)

            BiddingPhase.switchToBiddingPhase(Deck(cards), nextPlayers, scoreBoard)
          } else {
            PartnerUpPhase(newBiddingRound, dog, scoreBoard)
          }
        } else {
          replyTo ! PlacedBid(player, bidValue, isBidValid = true)
          nextBid(nextRound.get)
        }
      }
    case AskWhoIsNext(replyTo) =>
      val nextPlayer = biddingRound.getNextPlayer

      replyTo ! NextPlayer(nextPlayer, PhaseName.BIDDING_PHASE)
      Behaviors.same
    case GetPhaseName(replyTo) =>
      replyTo ! ReceivedPhaseName(PhaseName.BIDDING_PHASE)
      Behaviors.same
    case _ =>
      logger.error("Unhandled message")
      Behaviors.unhandled
  }
}

object BiddingPhase {
  def switchToBiddingPhase(deck: Deck, players: List[Player], scoreBoard: ScoreBoard): Behavior[Command] = {
    val dealtHands = deck.distributeCards(players)
    val biddingRound = BiddingRound(dealtHands.playerHands, List())

    BiddingPhase(dealtHands.dog, scoreBoard).nextBid(biddingRound)
  }
}