package tarot.business.gamenight

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import tarot.business.BiddingRound.Bid
import tarot.business.Deck.Dog
import tarot.business.{GameTrick, GreenMat, ScoreBoard}
import tarot.business.Player.Hand
import tarot.business.gamenight.GameNight.{AskWhoIsNext, Command, GetPhaseName, GetPlayerHand, NextPlayer, PhaseName, ReceivedPhaseName, ReceivedPlayerHand, UpdateDog, UpdatedDog, ViewDog, ViewedDog}
import play.api.Logging

object DogPhase extends Logging {
  logger.info("Dog Phase starts")

  def apply(dog: Dog, winningBid: Bid, playerHands: List[Hand], scoreBoard: ScoreBoard): Behavior[Command] =
    Behaviors.receiveMessage {
      case ViewDog(replyTo) =>
        val shouldDogBeVisible = winningBid.shouldDogBeVisible

        logger.debug(s"ViewDog returns: ${if (shouldDogBeVisible) dog.cards else "---hidden---"}")
        replyTo ! ViewedDog(shouldDogBeVisible, if (shouldDogBeVisible) dog.cards else List(), winningBid.player)
        Behaviors.same
      case UpdateDog(player, newDog, replyTo) =>
        if (player != winningBid.player || !winningBid.shouldDogBeVisible) {
          logger.debug("UpdateDog failed")

          replyTo ! UpdatedDog(false)
          Behaviors.same
        } else {
          val updatedPlayerHands = playerHands.find(_.player == player)
            .filter(validateDog(newDog, dog, _))
            .map(updatePlayerHands(_, newDog, dog, playerHands))

          if (updatedPlayerHands.isDefined) {
            logger.debug(s"UpdateDog succeeded for $player")

            replyTo ! UpdatedDog(true)
            val greenMat = GreenMat(numberOfPlayers = playerHands.size)
            val gameTrick = GameTrick(updatedPlayerHands.get, List(), greenMat)

            GameRoundPhase(winningBid, newDog, playerHands.head.player, scoreBoard).nextTrick(gameTrick)
          } else {
            logger.debug(s"UpdateDog failed for $player - $newDog is invalid")

            replyTo ! UpdatedDog(false)
            Behaviors.same
          }
        }
      case AskWhoIsNext(replyTo) =>
        val nextPlayer = winningBid.player

        replyTo ! NextPlayer(nextPlayer, PhaseName.DOG_PHASE)
        Behaviors.same
      case GetPlayerHand(player, replyTo) =>
        logger.debug(s"Dog Phase - GetPlayerHand message received: $player")
        val playerHand = playerHands.find(_.player == player).getOrElse(Hand(player, List()))

        replyTo ! ReceivedPlayerHand(playerHand)
        Behaviors.same
      case GetPhaseName(replyTo) =>
        replyTo ! ReceivedPhaseName(PhaseName.DOG_PHASE)
        Behaviors.same
      case command: Command =>
        logger.error(s"Unhandled message: $command")
        Behaviors.unhandled
    }

  private def validateDog(newDog: Dog, originalDog: Dog, playerHand: Hand): Boolean = {
    newDog.isValid && newDog.cards.forall(card => originalDog.contains(card) || playerHand.cards.contains(card))
  }

  private def updatePlayerHands(leaderHand: Hand, newDog: Dog, originalDog: Dog, playerHands: List[Hand]) = {
    val newHand = (leaderHand.cards ++ originalDog.cards).filterNot(newDog.cards.contains(_))

    playerHands.map(hand => if (hand.player == leaderHand.player) Hand(leaderHand.player, newHand) else hand)
  }

}
