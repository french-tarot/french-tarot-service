package tarot.business

import tarot.business.Deck.{DealtHands, Dog}
import tarot.business.Player.Hand
import tarot.business.TarotCard.{TarotCard, TarotSuit}
import play.api.Logging

import scala.collection.mutable
import scala.util.Random

object Deck {
  private val invalidDogSuits = Set(TarotSuit.TRUMP, TarotSuit.JOKER)
  private val invalidDogCards = Set(
    TarotCard.HEART_KING, TarotCard.CLUB_KING, TarotCard, TarotCard.DIAMOND_KING, TarotCard.SPADE_KING)

  case class Dog(cards: List[TarotCard]) {
    def contains(card: TarotCard): Boolean = cards.contains(card)
    def isValid: Boolean =
      !cards.exists(card => invalidDogCards.contains(card)) &&
      !cards.exists(card => invalidDogSuits(card.suit))
  }

  case class DealtHands(playerHands: List[Hand], dog: Dog)
}

case class Deck(cards: List[TarotCard], randomSeed: Random = new Random) extends Logging {

  def distributeCards(players: List[Player]): DealtHands = {
    logger.info(s"Distributed cards for ${players.size} players")
    val dog = pickDog(players)
    val remainingCards = cards.filter(!dog.contains(_))
    val hands = distribute(players.map(Hand(_, List())), remainingCards)

    DealtHands(hands, dog)
  }

  private def numberOfCardsInDog(numberOfPlayers: Int) = numberOfPlayers match {
    case 3 => 6
    case 4 => 6
    case 5 => 3
  }

  private def pickDog(players: List[Player]): Dog = {
    val numberOfCardsToAdd = numberOfCardsInDog(players.size)
    val dog: mutable.Set[TarotCard] = mutable.Set() // TODO: Should not use a set

    while (dog.size < numberOfCardsToAdd) {
      dog.add(cards(randomSeed.nextInt(cards.length-1)))
    }
    Dog(dog.toList)
  }

  @scala.annotation.tailrec
  private def distribute(hands: List[Hand], remainingCards: List[TarotCard]): List[Hand] = {
    logger.debug(s"Distributing with ${remainingCards.size} cards remaining")
    if(remainingCards.size <= 0) {
      return hands
    }
    distribute(
      hands.tail ++ List(Hand(hands.head.player, hands.head.cards ++ remainingCards.take(3))),
      remainingCards.drop(3))
  }

}
