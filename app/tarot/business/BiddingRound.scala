package tarot.business

import tarot.business.BiddingRound.BidValue.BidValue
import tarot.business.BiddingRound.{Bid, BidValue}
import tarot.business.Player.Hand
import tarot.business.TarotCard.{CLUB_KING, DIAMOND_KING, HEART_KING, SPADE_KING, TarotCard}
import tarot.business.TarotCard.TarotSuit.{CLUB, DIAMOND, HEART, SPADE, TarotSuit}
import play.api.Logging

case class BiddingRound(playerHands: List[Hand], playerBids: List[Bid]) extends Logging {

  def isBiddingFinished: Boolean = playerHands.size == playerBids.size

  def didEveryonePass: Boolean = playerBids.map(_.value).forall(_ == BidValue.PASS)

  def getNextPlayer: Player = {
    val nextPlayerIndex = playerBids.size

    playerHands(nextPlayerIndex).player
  }

  private def rejectLowerNonPassBid(value: BidValue): Boolean = {
    value != BidValue.PASS && playerBids.nonEmpty && playerBids.map(_.value.id).max >= value.id
  }

  def placeNextBid(player: Player, value: BidValue): Option[BiddingRound] = {
    val nextPlayer = getNextPlayer

    if (playerBids.size == playerHands.size || player != nextPlayer || rejectLowerNonPassBid(value)) {
      return Option.empty
    }
    logger.info(s"Player $player bets $value")
    Option(BiddingRound(playerHands, playerBids ++ List(Bid(nextPlayer, value))))
  }

  def getWinningBid: Option[Bid] = {
    if (playerBids.map(_.value).forall(_ == BidValue.PASS)) {
      logger.info("Everyone passed")
      Option.empty
    } else {
      val leader = playerBids.maxBy(_.value)

      logger.info(s"$leader won the bidding round")
      Option(leader)
    }
  }
}

object BiddingRound {

  private val hiddenDogBids = List(BidValue.GARDE_SANS, BidValue.GARDE_CONTRE)

  object BidValue extends Enumeration {
    type BidValue = Value

    val PASS: BiddingRound.BidValue.Value = Value(0)
    val PETITE: BiddingRound.BidValue.Value = Value(10)
    val POUCE: BiddingRound.BidValue.Value = Value(20)
    val GARDE: BiddingRound.BidValue.Value = Value(40)
    val GARDE_SANS: BiddingRound.BidValue.Value = Value(80)
    val GARDE_CONTRE: BiddingRound.BidValue.Value = Value(160)
  }

  case class Bid(player: Player, value: BidValue, partner: Option[Player] = Option.empty) {
    def shouldDogBeVisible: Boolean = !hiddenDogBids.contains(value)
    def withPartner(suit: TarotSuit, playerHands: List[Hand]): Bid = {
      val newPartner = MAP_SUIT_TO_KING(suit)
        .map(king => playerHands
          .filter(playerHand => playerHand.cards.contains(king))
          .map(_.player))
        .map(_.head)

      Bid(player, value, newPartner)
    }
  }

  private def MAP_SUIT_TO_KING(suit: TarotSuit): Option[TarotCard] = suit match {
    case HEART => Option(HEART_KING)
    case DIAMOND => Option(DIAMOND_KING)
    case SPADE => Option(SPADE_KING)
    case CLUB => Option(CLUB_KING)
    case _ => Option.empty
  }

}