package tarot.business

import tarot.business.BiddingRound.{Bid, BidValue}
import tarot.business.Deck.Dog
import tarot.business.GreenMat.WinStack
import tarot.business.ScoreBoard.{GameRoundScore, PlayerScore}

object ScoreCalculator {

  def getInitialScores(players: List[Player]): GameRoundScore =
    GameRoundScore(players.map(PlayerScore(_, 0)), Bid(players.head, BidValue.PASS))

  def apply(winStacks: List[WinStack], dog: Dog, winningBid: Bid,
            players: List[Player], scoreBoard: ScoreBoard): ScoreBoard = {
    val previousTotalPoints = scoreBoard.gameRoundScores.headOption.getOrElse(getInitialScores(players))
    val currentRoundPoints = calculateRoundPoints(winStacks, dog, winningBid, players)
    val newTotalPoints = previousTotalPoints + currentRoundPoints

    ScoreBoard(List(newTotalPoints) ++ scoreBoard.gameRoundScores)
  }

  private def getPartner(winningBid: Bid) = {
    if (winningBid.partner.isDefined && winningBid.partner.get == winningBid.player) {
      Option.empty
    } else {
      winningBid.partner
    }
  }

  private def calculateRoundPoints(winStacks: List[WinStack], dog: Dog, winningBid: Bid,
                                   players: List[Player]): ScoreBoard.GameRoundScore = {
    val leader = winningBid.player
    val partner = getPartner(winningBid)
    val leaderCards = winStacks.filter(_.player == leader).flatMap(_.cards)
    val partnerCards = partner
      .map(partner => winStacks.filter(_.player != leader).filter(_.player == partner).flatMap(_.cards))
      .getOrElse(List())
    val leaderDogCards = if (winningBid.value != BidValue.GARDE_CONTRE) dog.cards else List()
    val allLeaderCards = leaderCards ++ leaderDogCards ++ partnerCards
    val leaderPoints = allLeaderCards.map(_.points).sum
    val numberOfBouts = allLeaderCards.count(_.isBout)
    val numberOfPointsNeeded = NUMBER_OF_POINTS_REQUIRED(numberOfBouts)
    val difference = leaderPoints - numberOfPointsNeeded
    val roundedDifference = Math.round(difference/10.0) * 10
    val contract = winningBid.value.id + Math.abs(roundedDifference)

    val leadTeam = Set(leader, partner.getOrElse(leader))
    val opposingTeamScores = players.filter(!leadTeam.contains(_))
      .map(player => PlayerScore(player, -Math.signum(roundedDifference) * contract))
    val leaderFactor = if (partner.isDefined) opposingTeamScores.size - 1 else opposingTeamScores.size
    val leaderScore = List(PlayerScore(leader, Math.signum(roundedDifference) * contract * leaderFactor))
    val partnerScore = partner.map(player => List(PlayerScore(player, Math.signum(roundedDifference) * contract)))
        .getOrElse(List())

    GameRoundScore(opposingTeamScores ++ leaderScore ++ partnerScore, winningBid)
  }

  private val NUMBER_OF_POINTS_REQUIRED = Map(
    0 -> 56,
    1 -> 51,
    2 -> 41,
    3 -> 36
  )
}
