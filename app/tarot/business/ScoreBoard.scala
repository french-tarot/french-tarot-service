package tarot.business

import tarot.business.BiddingRound.Bid
import tarot.business.ScoreBoard.GameRoundScore

case class ScoreBoard(gameRoundScores: List[GameRoundScore] = List())

object ScoreBoard {
  case class PlayerScore(player: Player, score: Double)
  case class GameRoundScore(playerScores: List[PlayerScore], winningBid: Bid) {
    def +(that: GameRoundScore): GameRoundScore = {
      require(playerScores.size == that.playerScores.size)

      val newScores = that.playerScores.map(playerScore => playerScore.player -> playerScore).toMap
      val newTotalScores = playerScores.map(previousPlayerScore =>
        PlayerScore(previousPlayerScore.player, previousPlayerScore.score + newScores(previousPlayerScore.player).score)
      )

      GameRoundScore(newTotalScores, that.winningBid)
    }
  }
}