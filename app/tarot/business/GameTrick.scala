package tarot.business

import tarot.business.GreenMat.WinStack
import tarot.business.Player.{Hand, PlayerMove}

case class GameTrick(playerHands: List[Hand], winStacks: List[WinStack], greenMat: GreenMat) {
  def greenMatCards: List[PlayerMove] = greenMat.cardsPlayed

  def nextPlayer: Player = playerHands.head.player

  def validatePlayerMove(playerMove: PlayerMove): Boolean =
    nextPlayer == playerMove.player && greenMat.validatePlayerMove(playerMove, playerHands.head)

  def playerMove(playerMove: PlayerMove): Option[GameTrick] = {
    Option.when(validatePlayerMove(playerMove)) {
      validPlayerMove(playerMove)
    }
  }

  def nextBiddingRoundPlayers(currentStarter: Player): List[Player] = {
    val players = playerHands.map(_.player)
    val indexStarter = players.indexOf(currentStarter)

    GameTrick.rotateListPlayer(players, indexStarter)
  }

  private def validPlayerMove(playerMove: PlayerMove): GameTrick = {
    val updatedMat = greenMat + playerMove
    val updatedPlayerHands = updatePlayerHands(playerMove)

    if (updatedMat.isTrickComplete) {
      val updatedWinStacks = updatedMat.winStacks
      val starter = updatedWinStacks.head.player
      val indexStarter = updatedPlayerHands.map(_.player).indexOf(starter)
      val orderedPlayerHands = GameTrick.rotateList(updatedPlayerHands, indexStarter)

      GameTrick(orderedPlayerHands, updatedWinStacks ++ winStacks, updatedMat)
    } else {
      GameTrick(updatedPlayerHands, winStacks, updatedMat)
    }
  }

  private def updatePlayerHands(playerMove: PlayerMove) = {
    val updatedPlayerHand = Hand(playerMove.player, playerHands.head.cards.filter(_ != playerMove.card))

    playerHands.tail ++ List(updatedPlayerHand)
  }

}

object GameTrick {
  private def rotateList(list: List[Hand], index: Int) = list.drop(index) ++ list.take(index) // TODO Review why/if this works
  def rotateListPlayer(list: List[Player], index: Int = 0): List[Player] = list.drop(index + 1) ++ list.take(index + 1)
}