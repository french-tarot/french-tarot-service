package tarot.business

import tarot.business.TarotCard.TarotCard
import tarot.business.TarotCard.TarotSuit.TarotSuit

case class Player(name: String)

object Player {

  case class PlayerMove(player: Player, card: TarotCard)

  case class Hand(player: Player, cards: List[TarotCard]) {
    def has(suit: TarotSuit): Boolean = cards.exists(_.suit == suit)

    def doesNotHave(suit: TarotSuit): Boolean = {
      !has(suit)
    }
  }

}
