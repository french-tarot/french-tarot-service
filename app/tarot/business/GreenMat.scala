package tarot.business

import tarot.business.GreenMat.WinStack
import tarot.business.Player.{Hand, PlayerMove}
import tarot.business.TarotCard.TarotSuit.TarotSuit
import tarot.business.TarotCard.{TarotCard, TarotSuit}
import play.api.Logging

case class GreenMat(cardsPlayed: List[PlayerMove] = List(),
                    numberOfPlayers: Int = 5)
  extends Logging {

  val isTrickComplete: Boolean = cardsPlayed.size.equals(numberOfPlayers)
  val winStacks: List[WinStack] = {
    if (isTrickComplete) {
      buildWinStacks(cardsPlayed)
    } else {
      List()
    }
  }

  def validatePlayerMove(playerMove: PlayerMove, playerHand: Hand): Boolean = {
    if (!playerHand.cards.contains(playerMove.card)) {
      logger.error(
        f"${playerMove.player.name} does not have the card ${playerMove.card} in $playerHand")
      return false
    }
    val removeMoveFromHand = Hand(playerHand.player, playerHand.cards.filter(_ != playerMove.card))

    cardsPlayed.headOption
      .map(_.card.suit)
      .forall(trickSuit =>
        playerMove.card.isJoker ||
          playsTheSuit(trickSuit, playerMove, removeMoveFromHand) ||
          playsTrump(trickSuit, playerMove, removeMoveFromHand) ||
          playsAnythingElse(trickSuit, removeMoveFromHand))
  }

  private def playsAnythingElse(trickSuit: TarotSuit, playerHand: Hand): Boolean = {
    playerHand.doesNotHave(trickSuit) && playerHand.doesNotHave(TarotSuit.TRUMP)
  }

  private def playsHigherTrump(playerMove: PlayerMove, playerHand: Hand): Boolean = {
    if (playerMove.card.isNotTrump) {
      return true
    }

    val maxPlayedRank = cardsPlayed.filter(_.card.isTrump).map(_.card.rank).maxOption.getOrElse(-1)
    val maxPlayerRank = playerHand.cards.map(_.rank).maxOption.getOrElse(-1)

    playerMove.card.rank > maxPlayedRank || maxPlayerRank < maxPlayedRank
  }

  private def playsTrump(trickSuit: TarotSuit, playerMove: PlayerMove, playerHand: Hand): Boolean = {
    playerMove.card.isTrump &&
      playerHand.doesNotHave(trickSuit) &&
      playsHigherTrump(playerMove, playerHand)
  }

  private def playsTheSuit(trickSuit: TarotSuit, playerMove: PlayerMove, playerHand: Hand): Boolean = {
    playerMove.card.suit == trickSuit && playsHigherTrump(playerMove, playerHand)
  }

  private def buildWinStacksNoJoker(moves: List[PlayerMove]): WinStack = {
    val trickSuit = moves.head.card.suit
    val trickWinner: Player = moves.filter(_.card.isTrump).maxByOption(_.card.rank)
      .getOrElse(moves.filter(_.card.suit == trickSuit).maxBy(_.card.rank)).player

    WinStack(trickWinner, moves.map(_.card))
  }

  private def buildWinStacks(allCardsPlayed: List[PlayerMove]): List[WinStack] = {
    val jokerStack: Option[WinStack] = allCardsPlayed.find(_.card.isJoker)
      .map(p => WinStack(p.player, List(p.card)))
    val winStacks: WinStack = buildWinStacksNoJoker(allCardsPlayed.filter(_.card.isNotJoker))

    jokerStack.map(jokerWinStack => List(winStacks, jokerWinStack)).getOrElse(List(winStacks))
  }

  def +(playerMove: PlayerMove): GreenMat = if (cardsPlayed.size == numberOfPlayers) {
    GreenMat(List(playerMove), numberOfPlayers = numberOfPlayers)
  } else {
    GreenMat(cardsPlayed ++ List(playerMove), numberOfPlayers = numberOfPlayers)
  }

}

object GreenMat {

  case class WinStack(player: Player, cards: List[TarotCard])

}